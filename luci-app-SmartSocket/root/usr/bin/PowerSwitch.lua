#!/usr/bin/lua

PSCMD_STATUS =  1
PSCMD_ON =      2
PSCMD_OFF =     3

RESET_SEC = 	5

if (arg[1] == nil) then
  print('usage: powerswitch [port]')
else
  port = arg[1]*1 - 1
  usb = require('libusb1')
  local handle = usb.open_device_with_vid_pid(0x16c0, 0x05dc);
  if (handle~=nil) then
    print('Device with pid 0x05DC initialised with success !!!')
    local requesttype = usb.LIBUSB_REQUEST_TYPE_VENDOR + usb.LIBUSB_RECIPIENT_DEVICE + usb.LIBUSB_ENDPOINT_IN
    usb.control_transfer(handle, requesttype, PSCMD_ON, 5*RESET_SEC, port, 8, 5000)
  else
    print('Device with pid 0x05dc initialisation failed !!!')
  end
end
