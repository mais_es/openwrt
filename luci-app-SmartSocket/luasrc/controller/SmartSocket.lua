-- Copyright 2015 MaiS_ES <mais_es@mail.ru>
-- Based on commands.lua by Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

module("luci.controller.SmartSocket", package.seeall)

function index()
	local uci = require "luci.model.uci".cursor()
	local cfg = "/etc/config/SmartSocket"
	if not nixio.fs.access(cfg) then nixio.fs.writefile(cfg, "") end
	if uci:get_all("SmartSocket", "UI") == nil then
                uci:section("SmartSocket", "UI", "UI", {clOffline='#ff0000', clOnline='#00ff00', iWidth='2px', iBorder='5', iCol='2'})
                uci:save("SmartSocket")
		uci:commit("SmartSocket")
        end

	entry({"admin", "services", "SmartSocket"}, firstchild(), _("Smart Socket"), 80).index = true
	entry({"admin", "services", "SmartSocket", "Dashboard"}, template("SmartSocket"), _("Dashboard"), 1)
	entry({"admin", "services", "SmartSocket", "Targets"}, cbi("SmartSocket/targets"), _("Configure Targets"), 2)
	entry({"admin", "services", "SmartSocket", "Config"}, cbi("SmartSocket/config"), _("Configure Module"), 3)
	entry({"admin", "services", "SmartSocket", "alarm"}, call("alarm"), nil).leaf = true
	entry({"admin", "services", "SmartSocket", "status"}, call("status"), nil).leaf = true
end

function alarm(id)
	if id == "0" then
		os.execute("/etc/init.d/apinger restart")
	else
		os.execute("PowerSwitch.lua " .. id)
	end
end

function status()
	local json = {}
	local s, ip = nil

	if luci.sys.init.enabled("apinger") then
		json['apinger'] = (os.execute("pidof apinger > /dev/null") == 0)
	end
	if nixio.fs.access("/tmp/apinger.status") then
		for line in io.lines("/tmp/apinger.status") do
			s = luci.util.split(line, " ")
			if s[1] == "Target:" then	
				ip = s[2]
			end
			if (s[1] == "Active") and ip then
				json[ip] = (s[3] == "None")
				ip = nil
			end
		end
	end
	luci.http.prepare_content("application/json")
	luci.http.write_json(json)
end
