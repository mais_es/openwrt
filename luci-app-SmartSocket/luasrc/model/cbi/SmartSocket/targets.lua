-- Copyright 2015 MaiS_ES <mais_es@mail.ru>
-- Based on commands.lua by Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

local m, s
local fs = require "nixio.fs"

m = Map("SmartSocket", translate("Smart Socket"), "")

s = m:section(TypedSection, "Targets", "")
s.template = "cbi/tblsection"
s.anonymous = true
s.addremove = true

v=s:option(ListValue, "idSocket", translate("Socket Number"))
for i = 1,8 do v:value(i) end

o = s:option(Value, "TargetIP", translate("Target IP"), "")
o.datatype = "ip4addr"
o.default = "127.0.0.1"

o = s:option(Value, "Description", translate("Target Description"), "")
o.default = translate("Description")
o.size = 85

m.on_after_commit = function(self)
    local apinger_conf = {}
    local Target_IP = {}
    table.insert(apinger_conf,'user "root"')
    table.insert(apinger_conf,'group "root"')
    table.insert(apinger_conf,'status { file "/tmp/apinger.status"; interval 15s }')
    table.insert(apinger_conf,'alarm default { command on "/usr/bin/PowerSwitch.lua %T"; repeat 5m 2 }')
    table.insert(apinger_conf,'alarm down "down" { time 2m }')
    table.insert(apinger_conf,'target default { interval 15s; alarms "down" }')
    self.uci:foreach( "SmartSocket", "Targets", function(item) 
        if (Target_IP[item.TargetIP] == nil and item.TargetIP ~= "127.0.0.1") then
            table.insert(apinger_conf,'target "' .. item.TargetIP .. '" { description "' .. item.idSocket .. ' ' .. item.Description .. '"; }') 
            Target_IP[item.TargetIP] = 1
        end
    end )
    table.insert(apinger_conf,"")
    fs.writefile("/etc/apinger.conf", table.concat(apinger_conf, "\n"))
end

return m
