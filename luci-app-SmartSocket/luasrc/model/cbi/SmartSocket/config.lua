-- Copyright 2015 MaiS_ES <mais_es@mail.ru>
-- Based on commands.lua by Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

local m

m = Map("SmartSocket", translate("Smart Socket"), "")

ui = m:section(TypedSection, "UI", translate("UI Config"))
ui.anonymous = true
ui.addremove = false

v=ui:option(ListValue, "iCol", translate("Columns"))
for i = 1,3 do v:value(i) end

v=ui:option(ListValue, "iWidth", translate("Border Width"))
for i = 1,5 do v:value(i .. "px") end

l=ui:option(ListValue, "iBorder", translate("Border Type"))
for i,v in ipairs({"Bottom","Top","Left","Right","Vertical","Horizontal","Around"}) do l:value(i, translate(v)) end

ui:option(Value, "clOnline", translate("Online")).template = "cbi/cvalue"
ui:option(Value, "clOffline", translate("Offline")).template = "cbi/cvalue"

b = ui:option(DummyValue, "iButton", " ")
b.inputtitle = translate("Reboot")
b.inputstyle = "reload"
b.template = "cbi/ssBorder"

return m
