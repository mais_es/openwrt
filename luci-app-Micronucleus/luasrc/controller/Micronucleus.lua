-- Copyright 2015 MaiS_ES <mais_es@mail.ru>
-- Based on system.lua by Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

module("luci.controller.Micronucleus", package.seeall)

function index()
	entry({"admin", "services", "Micronucleus"}, call("action"), _("Micronucleus"), 99)
end

function action()
	local log = "/tmp/micronucleus.log"
	local fw = "/tmp/micronucleus.hex"
	local bin = "/usr/bin/micronucleus"
	local devdir = "/usr/bin/micronucleus.dev/"
	local fs = require "nixio.fs"
	
	local d, fp, err, res, cmd, dev
	local devs = {}
	if fs.access(devdir) then
	        for d in fs.dir(devdir) do
	                table.insert(devs, d)
	        end
	        table.sort(devs)
	end

	fs.remove(log)	
	fs.remove(fw)
	
	luci.http.setfilehandler(
		function(meta, chunk, eof)
			if not fp then
				if meta and meta.name == "firmware" then
					fp = io.open(fw, "w")
				end
			end
			if fp then
				if chunk then fp:write(chunk) end
				if eof then fp:close() end
			end
		end
	)

	err = nil
	cmd = nil
	if fs.access(bin) then
		if luci.http.formvalue("clear") then
			cmd = bin.." --erase-only --timeout 5"
		elseif luci.http.formvalue("flash") then
			if fs.access(fw) then
				cmd = bin.." --type intel-hex --run --timeout 5 "..fw
			else
				err = "Firmware file not selected ..."
			end
		end
	else
		err = "Micronucleus flasher doesn't exist !!!"
	end

	dev = luci.http.formvalue("dev") or nil
	if cmd then
		if dev and #dev > 0 then
			os.execute(devdir..dev.." >> "..log)
		end
		if cmd then
			os.execute(cmd..'|grep -v "% complete" >> '..log)
		end
	end
	res = fs.readfile(log) or nil
	luci.template.render("Micronucleus", {err = err, res = res, dev = dev, devs = devs})
end
