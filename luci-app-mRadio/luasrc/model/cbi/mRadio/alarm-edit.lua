-- Copyright 2016 MaiS_ES <mais_es@mail.ru>
-- Based on commands.lua by Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

m = Map("mRadio", "")
m.redirect = luci.dispatcher.build_url("admin/network/mRadio/Alarms")

s = m:section(NamedSection, arg[1], "Alarm", "","" )
s.anonymous = true
s.addremove = false

v=s:option(ListValue, "iURL", translate("Source"))
v:value('')
m.uci:foreach("mRadio", "Inputs", function(s)
	v:value(s.iURL,s.iName)
end)

s:option(Value, "volume", translate("Volume")).template = "cbi/mRadio/volume"

v=s:option(ListValue, "timer", translate("Timer, min"))
v:value('')
for _,val in pairs(require("luci.mRadio").timers) do
	v:value(val)
end

b=s:option(Button, "", translate("Run Alarm"))

m.on_save = function(self)
	if self:formvalue("cbid.mRadio."..s.section..".volume") then
		if self:formvalue("cbid.mRadio."..s.section..".") then
			os.execute("/usr/bin/mRadio.alarm "..s.section)
		else
			luci.http.redirect(self.redirect)
		end
	end
end

return m
