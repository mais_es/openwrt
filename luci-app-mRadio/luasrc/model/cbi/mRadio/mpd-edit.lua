-- Copyright 2016 MaiS_ES <mais_es@mail.ru>
-- Based on commands.lua by Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

m = Map("mRadio", "")
m.redirect = luci.dispatcher.build_url("admin/network/mRadio/MPDs")

s = m:section(NamedSection, arg[1], "MPDs", "","" )
s.anonymous = true
s.addremove = false

o = s:option(Value, "host", translate("MPD Host"))
o.rmempty = true
o.placeholder = '[Server][:port]'

o = s:option(Value, "pwd", translate("Password"))
o.rmempty = true

o = s:option(Value, "icon", translate("Icon"), "")
o.template    = "cbi/mRadio/icon_list"
o.nocreate    = true
o.unspecified = true

return m
