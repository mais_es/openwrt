-- Copyright 2016 MaiS_ES <mais_es@mail.ru>
-- Based on commands.lua by Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

local m = Map("mRadio", "", "")

s = m:section(TypedSection, "Inputs", "")
s.template = "cbi/tblsection"
s.anonymous = true
s.addremove = true
s.sortable = true

o = s:option(Value, "iName", translate("Input Name"), "")

o = s:option(Value, "iURL", "URL", "")
o.size = '50%'
m.on_before_commit = function(self)
	local iCfg = (self.uci:get("mRadio", "Config", "iCfg") or 0) + 1
	self.uci:set("mRadio", "Config", "iCfg", iCfg)
end

return m
