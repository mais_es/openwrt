-- Copyright 2016 MaiS_ES <mais_es@mail.ru>
-- Based on commands.lua by Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

local m = Map("mRadio", "", "")

s = m:section(TypedSection, "Api", "")
s.template = "cbi/tblsection"
s.anonymous = true
s.addremove = true
s.sortable = true

o = s:option(Value, "ownek", translate("Key Owner"), "")

o = s:option(Value, "key", translate("Api key"), "")

return m
