-- Copyright 2016 MaiS_ES <mais_es@mail.ru>
-- Based on commands.lua by Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

local m
dt = require("luci.cbi.datatypes")
math.randomseed(os.time())

m = Map("mRadio", "", "")

s = m:section(TypedSection, "MPDs", "")
s.template = "cbi/tblsection"
s.anonymous = true
s.addremove = true
s.sortable = true
s.extedit   = luci.dispatcher.build_url("admin/network/mRadio/MPDs/%s")

s:option(Flag, "enable", translate("Active"), "")

o = s:option(Value, "descr", translate("Description"))

---t = m:section(TypedSection, "Tokens", translate("API Tokens"))
---t.template = "cbi/tblsection"
---t.anonymous = true
---t.addremove = true

---t:option(Flag, "tEnable", translate("Active"), "")
---o = t:option(Value, "tName", translate("Owner"), "")
--o.size = '40%'

---o = t:option(Value, "tValue", translate("Token"), "")
--o.size = '40%'
---o.default = string.format('%08X',math.random(2^16-1))

m.on_after_commit = function(self)
	os.execute('test -f /etc/crontabs/root && grep -q "mRadio.timer" /etc/crontabs/root || echo "* * * * * /usr/bin/mRadio.timer -" >> /etc/crontabs/root')
	os.execute('/etc/init.d/cron restart')
end

return m

