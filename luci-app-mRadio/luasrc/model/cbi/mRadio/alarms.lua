-- Copyright 2016 MaiS_ES <mais_es@mail.ru>
-- Based on commands.lua by Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

local fs = require "nixio.fs" 

m = Map("mRadio", "", "")

s = m:section(TypedSection, "Alarms", "")
s.template = "cbi/tblsection"
s.anonymous = true
s.addremove = true
s.extedit   = luci.dispatcher.build_url("admin/network/mRadio/Alarms/%s")

s:option(Flag, "active", translate("Active"))
s:option(Value, "week", translate("Days of Week")).template = "cbi/mRadio/week"
s:option(Value, "time", translate("Time")).template = "cbi/mRadio/time"

v=s:option(ListValue, "host", translate("MPD Server"))
m.uci:foreach("mRadio", "MPDs", function(s)
	v:value(s.host or 'localhost',s.descr)
end)

m.on_after_commit = function(self)
	local ctf = io.open("/etc/crontabs/root", "r")
	local crons, ln, str, t, d, w = {}

	repeat
		ln = ctf:read("*l")
		if ln and not ln:match("mRadio.alarm") then
			crons[#crons+1] = ln
		end
	until not ln

	m.uci:foreach("mRadio","Alarms",
		function(s)
			if s.active and s.time and s.week then
				t = luci.util.split(s.time, ":")
				w = tonumber(s.week)
				local days = {}
				for i=6,0,-1 do
					if w >= 2^i then
						w = w - 2^i
						days[#days+1] = i
					end
				end

				if (#t == 2) and (#days > 0) then
					table.sort(days)
					crons[#crons+1] = '%s %s * * %s mRadio.alarm %s' % {t[2], t[1], table.concat(days,','), s['.name']}
				end
			end
		end
	)
	fs.writefile("/etc/crontabs/root",table.concat(crons,"\n").."\n")
	os.execute('/etc/init.d/cron restart')
end

return m
