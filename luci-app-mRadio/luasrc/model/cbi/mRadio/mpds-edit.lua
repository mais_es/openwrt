-- Copyright 2015 MaiS_ES <mais_es@mail.ru>
-- Based on commands.lua by Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

m = Map("mRadio", "")
m.redirect = luci.dispatcher.build_url("admin/network/mRadio/Alarms")

s = m:section(NamedSection, arg[1], "Alarm", "","" )
s.anonymous = true
s.addremove = false
s.template = "mRadio/mpds-edit"

m.on_init = function(self)
	cmd = string.format('/usr/bin/mRadio.alarm %s %s %s %s',
		self:get(s.section, "iURL") or '',
		self:get(s.section, "iTimer") or '-',
		self:get(s.section, "volume") or '-',
		self:get(s.section, "iFadein") or '-'
	)
	os.execute(cmd)
end

m.on_save = function(self)
	f = self:formvalue("iFadein")
	t = self:formvalue("iTimer")
	v = self:formvalue("volume")
	if f and t then
		self:set(s.section, "iFadein", f)
		self:set(s.section, "iTimer", t)
		if v then 
			self:set(s.section, "volume", v)
		end
		luci.http.redirect(luci.dispatcher.build_url("admin/network/mRadio/Alarms"))
	end
end

return m
