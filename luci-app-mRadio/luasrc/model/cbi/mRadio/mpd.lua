-- Copyright 2016 MaiS_ES <mais_es@mail.ru>
-- Based on commands.lua by Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

local m, amixer
local fs = require "nixio.fs"

m = Map("mRadio", "", "")

s = m:section(NamedSection, "mpd","")
s.addremove = false
s.anonymous = true 

o = s:option(Value, "port", translate("Port"))
o.datatype = "port"
o.rmempty = true
o.placeholder = 6600

o = s:option(Value, "pwd", translate("Password"))
o.rmempty = true

o = s:option(Value, "buf", translate("Buffer Size, kb"))
o.rmempty = true
o.datatype = "uinteger"
o.placeholder = 4096

o = s:option(Value, "prebuf", translate("Preload Buffer, %"))
o.rmempty = true
o.datatype = "range(1,100)"
o.placeholder = 10

o = s:option(Value, "device", translate("HW Device"))
o.rmempty = true
o.placeholder = "plughw:0,0"

v=s:option(ListValue, "mix_control", translate("Mixer Control"))
amixer = io.popen("amixer scontrols 2>&1")
if amixer then
	while true do
		ln = amixer:read("*l")
		if not ln then break end
		a = luci.util.split(ln,"'")
		if a[2] then v:value(a[2]) end
	end
end
amixer:close() 

s:option(Flag, "gapless", translate("Gapless Playback"))
s:option(Flag, "vol_norm", translate("Volume Normalization"))
s:option(Flag, "vol_soft", translate("Software Volume"))

m.on_after_commit = function(self)
	mpd = m.uci:get_all("mRadio", "mpd") or {}
fs.writefile("/etc/mpd.conf",table.concat({
	'port	"'..(mpd.port or '6600')..'"',
	(mpd.pwd and 'password	"'..mpd.pwd..'@' or 'default_permissions	"')..('read,add,control,admin')..'"',
	'audio_buffer_size	"'..(mpd.buf or '4096')..'"',
	'buffer_before_play	"'..(mpd.prebuf or 10)..'%"',
	'gapless_mp3_playback	"'..(mpd.gapless and 'yes' or 'no')..'"',
	'volume_normalization	"'..(mpd.vol_norm and 'yes' or 'no')..'"',
	'mixer_type		"'..(mpd.vol_soft and 'software' or 'hardware')..'"',
	'audio_output {',
	'	type	"alsa"',
	'	name	"Local"',
	'	device	"'..(mpd.device or 'plughw:0,0')..'"',
	'	mixer_control	"'..(mpd.mix_control or 'PCM')..'"',
	'}',
	''
	},"\n"))
	os.execute('/etc/init.d/mpd restart')
end

return m
