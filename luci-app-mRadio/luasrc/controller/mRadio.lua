-- Copyright 2016 MaiS_ES <mais_es@mail.ru>
-- Based on commands.lua by Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

module("luci.controller.mRadio", package.seeall)

local fs = require "nixio.fs"
local uci = require "luci.model.uci".cursor()
local mr = require("luci.mRadio")

local cfg = "/etc/config/mRadio"
if not fs.access(cfg) then fs.writefile(cfg, "") end
if uci:get_all("mRadio", "Config") == nil then
	uci:section("mRadio", "Config", "Config", {iCfg=0})
	uci:save("mRadio")
	uci:commit("mRadio")
end
if uci:get_all("mRadio", "mpd") == nil then
	uci:section("mRadio", "mpd", "mpd", {})
	uci:save("mRadio")
end

function index()
	local mpd = require("luci.mRadio").get_sec("MPDs")

	entry({"admin", "network", "mRadio"}, firstchild(), _("mRadio"), 80).index = true
	if (#mpd > 0) then
		entry({"admin", "network", "mRadio", "Dashboard"}, call("dashboard"), _("Dashboard"), 1).leaf = true
	end
	if require("nixio.fs").access("/etc/mpd.conf") then
		entry({"admin", "network", "mRadio", "mpd"}, cbi("mRadio/mpd"), _("Local MPD"), 2)
	end
	entry({"admin", "network", "mRadio", "MPDs"},  arcombine(cbi("mRadio/mpds"),cbi("mRadio/mpd-edit", {hideapplybtn=true,hideresetbtn=true})), _("Remote MPDs"), 3).leaf = true
	entry({"admin", "network", "mRadio", "Inputs"}, cbi("mRadio/inputs"), _("Inputs"), 4)
	entry({"admin", "network", "mRadio", "Keys"}, cbi("mRadio/keys"), _("API keys"), 5)
	entry({"admin", "network", "mRadio", "Alarms"}, arcombine(cbi("mRadio/alarms"),cbi("mRadio/alarm-edit", {hideapplybtn=true,hideresetbtn=true})), _("Alarms"), 6).leaf = true
	entry({"admin", "network", "mRadio", "cmd"}, call("cmd"), nil).leaf = true
	x = entry({"admin", "network", "mRadio", "api"}, call("api"), nil)
	x.leaf = true
	x.dependent = false
	x.sysauth = false
end

function dashboard(mpdsrv,...)
	luci.template.render("mRadio", {mpdsrv = mpdsrv})
end

function cmd(script,host,...)
	local json = {iCfg=(uci:get("mRadio", "Config", "iCfg") or 0),mpds = {}, ver='0.6.0-1'}

	uci:foreach("mRadio","MPDs", function(s)
		h0 = s.host or 'localhost'
		h1 = host or h0
		if s.enable and (h0 == h1) then 
			os.execute('/usr/bin/mRadio.' .. (script or 'mpd') .. ' ' .. h0 .. ' ' ..table.concat(arg, ' '))
			json['mpds'][h0] = mr.get_stat(h0)
			json['mpds'][h0].descr = s.descr
			json['mpds'][h0].icon = s.icon
		end
	end )
	luci.http.prepare_content("application/json")
	luci.http.write_json(json)
end

function api(key,script,...)
	local key_ok = false
	uci:foreach("mRadio","Api", function(s)
		if (key==s.key) then key_ok = true end
	end )
	if key_ok then
		if (script==nil) then
		 	luci.http.prepare_content("application/json")
			luci.http.write_json(mr.get_sec("Inputs"))
		else
			cmd(script,...)
		end
	end
end
