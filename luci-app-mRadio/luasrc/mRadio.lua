local fs = require("nixio.fs")
local util = require "luci.util"
local string = require "string"
local uci = require "luci.model.uci".cursor()
local os = require "os"
local pairs = pairs

module "luci.mRadio"

get_sec = function(sec)
	local r = {}
	uci:foreach("mRadio",sec, function(s) r[#r+1] = s end )
	return r
end

utf8 = function(s)
	local win={[147]='\226\128\156',[148]='\226\128\157',[161]='\208\142',[162]='\208\158',
		[168]='\208\129',[178]='\208\134',[179]='\208\150',[184]='\209\145',[185]='\226\132\150'}
	local r, prev, b = '', 0
	for i = 1, s and s:len() or 0 do
		b = s:byte(i)
		if (b == 195) or (b == 194) then
			prev = b
		else	
			if prev == 195 then
				if b > 127 and b < 160 then
					r = r..'\208'..string.char(b + 16)
				elseif b > 159 and b < 176 then
					r = r..'\208'..string.char(b + 16)
				elseif b > 175 and b < 192 then
					r = r..'\209'..string.char(b - 48)
				else
					r = r..'\195'..string.char(b)
				end	
			elseif prev == 194 then
				r = r..(win[b] or '\194'..string.char(b))
			else 
				r = r..string.char(b)
			end
			prev = 0
		end
	end
	return r
end

timers = {10,20,30,45,60,90,120,150,180}

get_stat = function(host)
	local sf = '/tmp/mRadio.'..host
	local r = {}
	os.execute('touch ' .. sf)
	for k, v in fs.readfile(sf):gmatch('(%w+)=([^\n]+)') do
		r[k] = v
	end
	return r
end

set_stat = function(host, stat)
	s = ''
	for k,v in pairs(stat) do s = s..k..'='..v..'\n' end
	fs.writefile('/tmp/mRadio.'..host, s)
end
